from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np

import tensorflow as tf

import tensorflow_hub as hub
import tensorflow_datasets as tfds

print("Version: ", tf.__version__)
print("Eager mode: ", tf.executing_eagerly())
print("Hub version: ", hub.__version__)
print("GPU is", "avaliable" if tf.config.experimental.list_physical_devices("GPU") else "NOT AVALIABLE")

# Vamos dividir a base de dados em 60% para treino e 40% para avaliação

train_data, validation_data, test_data = tfds.load(
    name = "imdb_reviews",
    split=('train[:60%]', 'train[60%:]', 'test'),
    as_supervised=True
)

print('---------- test_data ---------')

test_examples_batch, test_labels_batch = next(iter(test_data.batch(1)))
print(test_examples_batch, '\n', test_labels_batch)


# Explorando a base de dados
train_examples_batch, train_labels_batch = next(iter(train_data.batch(10)))
print(train_examples_batch)
print(train_labels_batch)


# Aproveitando uma rede pronta para processar o texto
embedding = "https://tfhub.dev/google/tf2-preview/gnews-swivel-20dim/1"
hub_layer = hub.KerasLayer(embedding, input_shape=[], dtype=tf.string, trainable=True)

print(hub_layer(train_examples_batch[:3]))

model = None
# Importando a rede neural caso exista
try:
    model = tf.keras.models.load_model('saved_models/')
    print('---- Modelo importado. Pulando fase de treinamento. ----')
except Exception:
    print('-- Não foi possível carregar o modelo. Iniciando treinamento. --')
    

    # Construindo o modelo da rede neural
    model = tf.keras.Sequential()
    model.add(hub_layer)
    model.add(tf.keras.layers.Dense(16, activation='relu'))
    model.add(tf.keras.layers.Dense(1))
    model.summary()

    model.compile(optimizer='adam',
                loss=tf.keras.losses.BinaryCrossentropy(from_logits=True),
                metrics=['accuracy'])

    history = model.fit(train_data.shuffle(10000).batch(512),
                        epochs=20,
                        validation_data=validation_data.batch(512),
                        verbose=1)

    # Salvando o modelo
    model.save('saved_models/', save_format='tf')



results = model.evaluate(test_data.batch(512), verbose=2)

for name, value in zip(model.metrics_names, results):
    print("%s:%.3f" % (name, value))


# value = np.array("I love this movie. Couldn\'t recommend it enough.")
# # np.append(value, "I love this movie. Couldn\'t recommend it enough.")
# predictions = model.predict( value, batch_size=1 )
# # item_no = 0
# # print('Resposta correta: ', test_labels_batch[item_no])
# print('Resposta da máquina: ', predictions)