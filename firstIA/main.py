from __future__ import absolute_import, division, print_function, unicode_literals

# TensorFlow e tf.keras
import tensorflow as tf
from tensorflow import keras

# Bibliotecas auxiliares
import numpy as np
# import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.pyplot as plt


# Importando a base de dados
fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()

# Enum com nomes das classes
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress',
'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']


plt.figure()
plt.imshow(train_images[0])
plt.colorbar()
plt.grid(False)
# plt.show()
plt.savefig('firstIA/output/item1.png', bbox_inches='tight')
plt.close()

# Pre-processamento das figuras
train_images = train_images/255.0
test_images = test_images/255.0

# Plot das primeiras 25 figuras pre-processadas
plt.figure(figsize=(10,10))
for i in range(25):
    plt.subplot(5,5,i+1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(train_images[i], cmap=plt.cm.binary)
    plt.xlabel(class_names[train_labels[i]])
plt.savefig('firstIA/output/pre-processed.png', bbox_inches='tight')
plt.close()


# Iniciando camadas da rede neural
model = keras.Sequential([
    keras.layers.Flatten(input_shape=(28,28)),
    keras.layers.Dense(128, activation='relu'),
    keras.layers.Dense(10, activation='softmax')
])

# Copnfigurações do modelo:
# Isso dará as características que procuramos
#   para que a máquina evolua
model.compile(
    optimizer='adam',
    loss='sparse_categorical_crossentropy',
    metrics=['accuracy']
)

# Treinando a rede neural
model.fit(train_images, train_labels, epochs=10)

# Testando a rede neural
test_loss, test_acc = model.evaluate(test_images, test_labels, verbose=2)
print('\nTest accuracy: ', test_acc)


# Tentando dar um label à cada peça de roupa
#  dos conjuntos de teste
print('Deseja mostrar os testes errados? [Y/n]')
ans = input()
if ans != 'n':
    predictions = model.predict(test_images)
    item_no = 0
    print(predictions[item_no])
    print('Resposta da máquina: ', class_names[np.argmax(predictions[item_no])] )
    print('Resposta certa: ', class_names[test_labels[item_no]])

    for i in range(len(predictions)):
        if np.argmax(predictions[i]) != test_labels[i]:
            plt.figure()
            plt.imshow(test_images[i])
            plt.colorbar()
            plt.grid(False)
            # plt.show()
            plt.savefig('firstIA/output/wrong_item'+str(i)+'.png', bbox_inches='tight')
            plt.close()
            print('Resposta da máquina para o item', i, ': ', class_names[np.argmax(predictions[i])] )
            print('Resposta certa: ', class_names[test_labels[i]])        