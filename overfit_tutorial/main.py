from __future__ import absolute_import, division, print_function, unicode_literals

import tensorflow as tf

from tensorflow.keras import layers
from tensorflow.keras import regularizers

import tensorflow_docs as tfdocs
import tensorflow_docs.modeling
import tensorflow_docs.plots

from IPython import display
from matplotlib import pyplot as plt

import numpy as np

import pathlib
import shutil
import tempfile


LOGDIR = pathlib.Path(tempfile.mkdtemp())/"tensorboard_logs"
FEATURES = 28

N_VALIDATION = int(1e3)
N_TRAIN = int (1e4)
BUFFER_SIZE = int(1e4)
BATCH_SIZE = 500
STEPS_PER_EPOCH = N_TRAIN//BATCH_SIZE

LR_SCHEDULE = None

VALIDATE_DS = None
TRAIN_DS = None

# Junta os escalares em uma tupla (feature_vector, label)
def pack_row(*row):
    label = row[0]
    features = tf.stack(row[1:], 1)
    return features, label

def get_optimizer():
    return tf.keras.optimizers.Adam(LR_SCHEDULE)

def get_callbacks(name):
    return [
        tfdocs.modeling.EpochDots(),
        tf.keras.callbacks.EarlyStopping(monitor='val_binary_crossentropy', patience=200),
        tf.keras.callbacks.TensorBoard(LOGDIR/name)
    ]


def compile_and_fit(model, name, optimizer=None, max_epochs=10000):
    if optimizer is None:
        optimizer = get_optimizer()

    model.compile(
                  optimizer=optimizer,
                  loss='binary_crossentropy',
                  metrics=['accuracy', 'binary_crossentropy'])

    model.summary()

    history = model.fit(
        TRAIN_DS,
        steps_per_epoch = STEPS_PER_EPOCH,
        epochs = max_epochs,
        validation_data = VALIDATE_DS,
        callbacks = get_callbacks(name),
        verbose = 0
    )
    return history


if __name__ == "__main__":

    shutil.rmtree(LOGDIR, ignore_errors=True)

    gz = tf.keras.utils.get_file('HIGGS.csv.gz', 'https://archive.ics.uci.edu/ml/machine-learning-databases/00280/HIGGS.csv.gz')

    ds = tf.data.experimental.CsvDataset(gz, [float(),]*(FEATURES+1), compression_type="GZIP")

    packed_ds = ds.batch(10000).map(pack_row).unbatch()


    for features, label in packed_ds.batch(1000).take(1):
        print(features[0])
        plt.hist(features.numpy().flatten(), bins = 101)
        plt.savefig('overfit_tutorial/output/packet_ds_Hist.png', bbox_inches='tight')
        plt.close()

    
    VALIDATE_DS = packed_ds.take(N_VALIDATION).cache()
    TRAIN_DS = packed_ds.skip(N_VALIDATION).take(N_TRAIN).cache()

    print('Dados de treinamento: ', TRAIN_DS)

    VALIDATE_DS = VALIDATE_DS.batch(BATCH_SIZE)
    TRAIN_DS = TRAIN_DS.shuffle(BUFFER_SIZE).repeat().batch(BATCH_SIZE)

    # ----------------------- Treinamento -----------------------

    # Diminuir a taxa de aprendizado ao longo do tempo
    LR_SCHEDULE = tf.keras.optimizers.schedules.InverseTimeDecay(
        0.001,
        decay_steps=STEPS_PER_EPOCH*1000,
        decay_rate=1,
        staircase=False
    )

    # Mostrar taxa de aprendizado
    step = np.linspace(0,100000)
    lr = LR_SCHEDULE(step)
    plt.figure(figsize=(8,6))
    plt.plot(step/STEPS_PER_EPOCH, lr)
    plt.ylim([ 0, max(plt.ylim()) ])
    plt.xlabel('Epoch')
    plt.ylabel('Learning Rate')
    plt.savefig('overfit_tutorial/output/learningByEpoch.png', bbox_inches='tight')



    print('Optimizer: ', get_optimizer())

    # Começando com um modelo linear
    tiny_model = tf.keras.Sequential([
        layers.Dense(16, activation='elu', input_shape=(FEATURES, )),
        layers.Dense(1, activation='sigmoid')
    ])
    size_histories = {}
    size_histories['Tiny'] = compile_and_fit(tiny_model, 'sizes/Tiny')

    # Gráfico de desempenho
    plotter = tfdocs.plots.HistoryPlotter(metric = 'binary_crossentropy', smoothing_std=10)
    plotter.plot(size_histories)
    plt.ylim([0.5, 0.7])
    plt.savefig('overfit_tutorial/output/resultTiny', bbox_inches='tight')
    plt.close()


    # Modelo um pouco maior (Small)
    small_model = tf.keras.Sequential([
        # 'input_shape' só é necessácio aqui para que '.summary'funcione
        layers.Dense(16, activation='elu', input_shape=(FEATURES, )),
        layers.Dense(16, activation='elu'),
        layers.Dense(1, activation='sigmoid')
    ])

    size_histories['Small'] = compile_and_fit(small_model, 'sizes/Small')

    plotter = tfdocs.plots.HistoryPlotter(metric = 'binary_crossentropy', smoothing_std=10)
    plotter.plot(size_histories)
    plt.ylim([0.5, 0.7])
    plt.savefig('overfit_tutorial/output/FinalResult', bbox_inches='tight')
    plt.close()